#!/bin/sh
execute() { 
	clear; 
	java -Djava.awt.headless=true -jar ./target/cornswallop-1.0-SNAPSHOT-jar-with-dependencies.jar src/main/resources/samples/index.csf | tee run.out;
}
if [[ $(find src -newer run.out) ]]; then
  mvn package && execute
else
  execute
fi
