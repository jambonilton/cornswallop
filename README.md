# Cornswallop

A simplified UI Framework for text adventures and the like.

## Running / Building

This project is built with maven.  To run, just run "run.sh" and it will build automatically.

## Syntax

Below is the general syntax for a cornswallop file:

```
<attr-key>:<attr-value>
<attr-key>:<attr-value>
...

<text>

- <option>
    <attr-key>:<attr-value>
    <attr-key>:<attr-value>
    ...
- <option>
    <attr-key>:<attr-value>
    <attr-key>:<attr-value>
    ...
...

```

Note that a file maps to a "node", which consists of key-value attributes, some text and a list of options.  Each 
option also has a list of key-value attributes.  Using the attributes, text, options and file links, cornswallop can 
manage a complex, branching UI structure.

Here's a sample of a node with some links in it to other files:
```
type:adventure

In the distance you see a bear.

- Play dead.
    node: type: exit; As it turns out, this bear was smarter than average.  Your last moments in life are lived out 
          failing to play dead as your insides are gobbled up by the bear.
- Attack!
    exit: You charge at the bear head on.  It begins fleeing, but in a few moments realizes that you are unarmed.  It 
          turns about and wastes no time in mauling you.
- Offer it a career.
    link: job_offer.csf
```

## Options

Options have text and attributes like nodes.  Below are the different attributes that are supported:

- **link:** links to another file.
- **exit:** exits the application after displaying the given text.
- **node:** displays a sub-node with no declared options. Attributes for the node are input as key-value pairs, split by 
          colons, and delimited with semi-colons.
- **if:** apply some conditional before accepting the given option.  This option must also be followed by and **else** and 
        optionally, **else-if**.

## Node Types

UI Nodes can have different types to indicate to the application how it should be displayed to the user.  They are set 
with the attribute **type** at the top of the node.

- **select:** select from a list of options.
- **adventure:** guess from a list of options.  Synonyms are also accepted.
- **exit:** end of the road. Prints the text value of the node before quitting.

## Node Attributes

As well as the **type** attribute, there are several other kinds that can be used in conjunction to create different 
side-effects.

- **script:** execute the given script when this node is encountered.

## Scripting

Within the text body of any node, script can be embedded.  By default, we use the Nashorn Javascript interpreter.  To 
embed a script in a node body, you simply open the script section with `${` and close it with `}`.

For example:
```
You check to see what you're holding.  You are holding ${ this['weapon'] ? 'a ' + weapon : 'nothing' }.
```

In the example, if the variable `weapon` is set, then it will output the weapon the user is holding, else it will say 
they are holding nothing.  Note that Nashorn will throw an error if you try to read an undefined variable like `weapon` 
without referencing it from the context like the above `this['weapon']`.

## Including

Anywhere in the text of a node, you can include other files using an **include** tag.

For example:
```
Here is the output of another file: $include(other_file.txt)
```

## Attributes

The following attributes can apply to both nodes or options, yielding different effects during the execution of the 
program.

- **sound:** play a sound from file.
- **music:** loop a sound file continuously.