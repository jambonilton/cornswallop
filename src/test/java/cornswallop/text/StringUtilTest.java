package cornswallop.text;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class StringUtilTest {

    @Test
    public void isEmpty() {
        assertTrue(StringUtil.isEmpty(""));
        assertTrue(StringUtil.isEmpty(null));
        assertFalse(StringUtil.isEmpty(" "));
    }

    @Test
    public void emptyWrap() {
        assertWrapped(null, null, 1);
        assertWrapped("", "", 1);
    }

    @Test
    public void nowrap() {
        assertWrapped("This is a test.", "This is a test.", 0);
        assertWrapped("This is a test.", "This is a test.", 100);
    }

    @Test
    public void wraps() {
        assertWrapped("This\nis a\ntest.", "This is a test.", 5);
        assertWrapped("This\nis a\ntest\n.", "This is a test.", 4);
    }

    @Test
    public void wrapsOnSpace() {
        assertWrapped("This\nis a\ntest.", "This is a test.", 6);
    }

    private void assertWrapped(String expected, String actual, int lineSize) {
        assertEquals(expected, StringUtil.wrap(actual, lineSize));
    }

    @Test
    public void trimBefore() {
        assertEquals("", StringUtil.trimBefore(""));
        assertEquals("test  \t", StringUtil.trimBefore("test  \t"));
        assertEquals("test  \t", StringUtil.trimBefore("\t  \n test  \t"));
    }

    @Test
    public void unwrap() {
        assertEquals("This is a test.", StringUtil.unwrap("This \nis a \n test."));
        assertEquals("This is a \n\ntest.", StringUtil.unwrap("This \nis a \n\ntest."));
    }

}