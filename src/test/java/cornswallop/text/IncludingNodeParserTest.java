package cornswallop.text;

import cornswallop.file.AppLinker;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;

import static org.junit.Assert.assertEquals;

public class IncludingNodeParserTest {

    private static final String TEST_STRING = "TEST";

    File file;
    AppLinker linker;
    IncludingNodeParser parser;

    @Before
    public void setUp() throws Exception {
        file = File.createTempFile("test", "txt");
        try (Writer writer = new FileWriter(file)) {
            writer.write(TEST_STRING);
        }
        linker = location -> file.toPath();
        parser = new IncludingNodeParser(new CsfNodeParser(), linker);
    }

    @Test
    public void importsFile() {
        final String input = "This is a $include(test.txt).";
        assertEquals("This is a TEST.", parser.parse(input).text());
    }

}