package cornswallop.text;

import cornswallop.model.AppNode;
import cornswallop.model.AppOption;
import org.junit.Test;

import java.io.File;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static cornswallop.util.CollectionUtil.toStream;
import static org.junit.Assert.assertEquals;

public class CsfNodeParserTest {

    private CsfNodeParser reader = new CsfNodeParser();

    @Test
    public void parsesNodeWithoutAttributes() {
        final AppNode node = reader.parse("This is a test.\n - option 1\n  foo:bar\n  dude:car\n - option 2\n  a:1\n  b:2");
        assertEquals("This is a test.", node.text());
        final Iterator<AppOption> options = node.iterator();
        assertOptionMatches(options.next(), "option 1", "foo", "bar", "dude", "car");
        assertOptionMatches(options.next(), "option 2", "a", "1", "b", "2");
    }

    @Test
    public void parsesNodeWithAttributes() {
        final AppNode node = reader.parse("node-attribute:test\nThis is a test.\n - option 1\n  foo:bar\n  dude:car\n - option 2");
        assertEquals("This is a test.", node.text());
        assertEquals("test", node.get("node-attribute"));
        final Iterator<AppOption> options = node.iterator();
        assertOptionMatches(options.next(), "option 1", "foo", "bar", "dude", "car");
    }

    @Test
    public void parsesConditional() {
        final AppNode node = reader.parse(new File("src/main/resources/samples/bear/bear.csf"));
        final AppOption conditional = toStream(node).filter(n -> n.get("if") != null).findAny().get();
        final Set<String> keys = Stream.of("if", "else if", "else").collect(Collectors.toSet());
        assertEquals(3, conditional.attributes().map(Map.Entry::getKey).filter(keys::contains).count());
    }

    private void assertOptionMatches(AppOption option, String text, String... attrs) {
        assertEquals(text, option.text());
        for (int i=0; i < attrs.length; i+=2)
            assertEquals(attrs[i+1], option.get(attrs[i]));
    }

}