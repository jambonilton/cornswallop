package cornswallop.script;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MoneySignBracesTemplateEvaluatorTest {

    private ScriptRunner runner = new NashornScriptRunner();
    private TemplateEvaluator evaluator = new MoneySignBracesTemplateEvaluator();

    @Test
    public void basicExample() {
        assertTemplated("${'test'}", "test");
    }

    @Test
    public void mixed() {
        assertTemplated("This is a ${'test'}. The sum of two twos is ${2+2}.", "This is a test. The sum of two twos is 4.");
    }

    @Test
    public void braceNesting() {
        runner.put("works", true);
        assertTemplated("Test works? ${if (works) { 'Huzzah!'; } else { 'Fuck!'; }}", "Test works? Huzzah!");
    }

    @Test
    public void undefined() {
        final String input = "'test' is ${this['test'] ? test : 'undefined'}";
        assertTemplated(input, "'test' is undefined");
        runner.put("test", "good");
        assertTemplated(input, "'test' is good");
    }

    private void assertTemplated(String input, String expected) {
        final String actual = evaluator.run(runner, input);
        assertEquals(expected, actual);
    }

}