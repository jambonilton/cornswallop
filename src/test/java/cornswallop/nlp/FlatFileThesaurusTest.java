package cornswallop.nlp;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class FlatFileThesaurusTest {

    private static final File FILE = new File("src/main/resources/mthes/mobythes.aur");

    @Test
    public void test() throws IOException {
        final FlatFileThesaurus thesaurus = new FlatFileThesaurus(FILE);
        assertEquals(319, thesaurus.lookup("mad").count());
        assertEquals(1, thesaurus.lookup("cookie").count());
        assertEquals(161, thesaurus.lookup("tree").count());
    }

}