package cornswallop.connector;

import cornswallop.model.AppNode;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SubNodeConnectorTest {

    SubNodeConnector subNodeConnector = new SubNodeConnector();

    @Test
    public void parsesNode() {
        final AppNode node = subNodeConnector.parseNode("script: weapon='stick';\n"
                + "You reach down and pick up the sturdiest-looking stick you can find.");
        assertEquals("weapon='stick';", node.get("script"));
    }

}