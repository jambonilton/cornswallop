package cornswallop;

import cornswallop.attributes.AttributeInterpreter;
import cornswallop.attributes.MusicInterpreter;
import cornswallop.attributes.SoundInterpreter;
import cornswallop.connector.*;
import cornswallop.media.AudioPlayer;
import cornswallop.media.BasicAudioPlayer;
import cornswallop.model.AppNode;
import cornswallop.model.AppOption;
import cornswallop.model.AppState;
import cornswallop.nlp.FlatFileThesaurus;
import cornswallop.nlp.OverrideThesaurus;
import cornswallop.nlp.Thesaurus;
import cornswallop.script.MoneySignBracesTemplateEvaluator;
import cornswallop.terminal.*;
import cornswallop.text.CsfOptionParser;
import cornswallop.util.Maps;

import java.io.File;
import java.util.Map;

public class Main {

    public static void main(String[] args) throws Exception {

        final Terminal terminal = new DefaultTerminal();
        final AudioPlayer audioPlayer = new BasicAudioPlayer();
        final Thesaurus thesaurus = new OverrideThesaurus(new FlatFileThesaurus(new File("src/main/resources/mthes/mobythes.aur")))
                .override("quit");
        final AppOption quit = AppOption.create("quit").onKey('q').attr("exit", "Goodbye!").build();
        final AppState state = AppState.create(validate(args))
                .addOption(quit);
        final Map<String, AttributeInterpreter> attributeInterpreters = Maps.hashMap(
                "sound", new SoundInterpreter(audioPlayer),
                "music", new MusicInterpreter(audioPlayer));
        final Map<String, AppConnector> connectors = Maps.hashMap(
                "exit", new ExitConnector(),
                "link", new LinkConnector(state, state),
                "node", new SubNodeConnector());
        final AppConnector connector = new SplitConnector(attributeInterpreters, connectors);
        connectors.put("if", new ConditionalConnector(connector, new CsfOptionParser()));

        final TerminalView view = new TerminalView(terminal,
                new MoneySignBracesTemplateEvaluator(),
                new AdventureTermInput(terminal, thesaurus),
                new SelectTermInput(terminal));

        while (true) {
            final AppNode next = connector.getNext(state, view.getInput(state));
            if (state.current() != next) {
                next.forAttributes((k,v)->{
                    if (attributeInterpreters.containsKey(k))
                        attributeInterpreters.get(k).execute(state, v);
                });
                state.push(next);
            }
        }

    }

    private static File validate(String[] args) {
        if (args.length != 1) {
            System.err.println("Usage: cornswallop <file>");
            System.exit(1);
        }
        final File file = new File(args[0]);
        if (!file.exists()) {
            System.exit(1);
            System.err.println("File \"" + file.getName() + "\" does not exist!");
        }
        return file;
    }

}
