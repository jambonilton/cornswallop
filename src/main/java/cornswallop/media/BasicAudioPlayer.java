package cornswallop.media;

import cornswallop.util.cache.Cache;

import javax.sound.midi.*;
import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

public class BasicAudioPlayer implements AudioPlayer {

    private static final String MIDI_EXT = "mid";

    private final Cache<File, Playback> sounds;

    public BasicAudioPlayer() {
        sounds = Cache.create(BasicAudioPlayer::toPlayback);
    }

    @Override
    public void play(File file) {
        sounds.get(file).play();
    }

    @Override
    public void loop(File file) {
        sounds.get(file).loop();
    }

    @Override
    public void stop(File file) {
        sounds.get(file).stop();
    }

    private static Playback toPlayback(File file) {
        try {
            final Clip clip = AudioSystem.getClip();
            if (file.getName().endsWith(MIDI_EXT)) {
                Sequencer sequencer = MidiSystem.getSequencer();
                sequencer.open();
                Sequence mySeq = MidiSystem.getSequence(file);
                sequencer.setSequence(mySeq);
                return new MidiPlayback(sequencer);
            }
            final AudioInputStream ais = AudioSystem.getAudioInputStream(file);
            clip.open(ais);
            clip.setFramePosition(0);
            return new SamplePlayback(clip);
        } catch (LineUnavailableException | UnsupportedAudioFileException | MidiUnavailableException
                | IOException | InvalidMidiDataException e) {
            throw new RuntimeException(e);
        }
    }

    interface Playback {
        void play();
        void loop();
        void stop();
    }

    static class SamplePlayback implements Playback {

        final Clip clip;

        public SamplePlayback(Clip clip) {
            this.clip = clip;
        }

        @Override
        public void play() {
            clip.start();
        }

        @Override
        public void loop() {
            if (!clip.isRunning())
                clip.loop(Clip.LOOP_CONTINUOUSLY);
        }

        @Override
        public void stop() {
            clip.stop();
            clip.close();
        }

    }

    static class MidiPlayback implements Playback {

        final Sequencer sequencer;

        public MidiPlayback(Sequencer sequencer) {
            this.sequencer = sequencer;
        }

        @Override
        public void play() {
            sequencer.start();
        }

        @Override
        public void loop() {
            if (!sequencer.isRunning()) {
                sequencer.setLoopCount(Sequencer.LOOP_CONTINUOUSLY);
                sequencer.start();
            }
        }

        @Override
        public void stop() {
            sequencer.stop();
        }
    }

}
