package cornswallop.media;

import java.io.File;

public interface AudioPlayer {

    void play(File file);

    void loop(File file);

    void stop(File file);

}
