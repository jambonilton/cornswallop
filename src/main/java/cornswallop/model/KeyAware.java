package cornswallop.model;

/**
 * For mapping things to keys in the UI.
 */
public interface KeyAware {

    int getKey();

}
