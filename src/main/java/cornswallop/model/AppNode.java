package cornswallop.model;

import cornswallop.text.StringUtil;

import java.util.*;
import java.util.function.BiConsumer;

import static cornswallop.text.StringUtil.unwrap;

/**
 * Junction of choice for the user.  Contains text, options, and attributes.
 */
public interface AppNode extends Iterable<AppOption> {

    String text();
    String get(String attribute);
    boolean hasOptions();
    void forAttributes(BiConsumer<String, String> consumer);

    static AppNodeBuilder builder(String text) {
        return new AppNodeBuilder(unwrap(text));
    }

    class AppNodeBuilder {
        final Map<String, String> attributes = new TreeMap<>();
        final Collection<AppOption> options = new ArrayList<>();
        final String text;

        public AppNodeBuilder(String text) {
            this.text = text;
        }

        public AppNodeBuilder attr(String key, String value) {
            attributes.put(key, value);
            return this;
        }

        public AppNodeBuilder attrs(Map<String, String> attrs) {
            attributes.putAll(attrs);
            return this;
        }

        public AppNodeBuilder add(AppOption appOption) {
            options.add(appOption);
            return this;
        }

        public AppNode build() {
            return new AppNode() {
                @Override
                public String text() {
                    return StringUtil.isEmpty(text) ? "" : text;
                }
                @Override
                public String get(String attribute) {
                    return attributes.get(attribute);
                }
                @Override
                public boolean hasOptions() {
                    return !options.isEmpty();
                }
                @Override
                public Iterator<AppOption> iterator() {
                    return options.iterator();
                }
                @Override
                public void forAttributes(BiConsumer<String, String> consumer) {
                    attributes.forEach(consumer);
                }
            };
        }
    }

}
