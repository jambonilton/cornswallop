package cornswallop.model;

import cornswallop.file.AppLinker;
import cornswallop.file.BasicLinker;
import cornswallop.script.NashornScriptRunner;
import cornswallop.script.ScriptRunner;
import cornswallop.text.IncludingNodeParser;
import cornswallop.text.CsfNodeParser;
import cornswallop.text.Parser;
import cornswallop.text.TextInput;
import cornswallop.util.LinkStack;
import cornswallop.util.Stack;

import javax.script.ScriptException;
import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Hodge-podge of important state info for the application.
 */
public interface AppState extends Stack<AppNode>, Iterable<AppOption>, AppLinker, ScriptRunner, Parser<AppNode> {

    static AppState create(File file) {
        final BasicLinker linker = new BasicLinker(file.toPath().getParent());
        final Parser<AppNode> csfParser = new IncludingNodeParser(new CsfNodeParser(), linker.immutable());
        return new BasicAppState(new NashornScriptRunner(), csfParser, csfParser.parse(file), linker);
    }

    AppState back();

    AppState addOption(AppOption option);

    class BasicAppState implements AppState {

        private final List<AppOption> options = new ArrayList<>();
        private final Stack<AppNode> stack;
        private final ScriptRunner scriptRunner;
        private final Parser<AppNode> parser;
        private final AppLinker linker;

        public BasicAppState(ScriptRunner scriptRunner,
                             Parser<AppNode> parser,
                             AppNode root,
                             AppLinker linker) {
            this.scriptRunner = scriptRunner;
            this.parser = parser;
            this.stack = new LinkStack<>(root);
            this.linker = linker;
        }

        @Override
        public Path resolve(String path) {
            return linker.resolve(path);
        }

        @Override
        public AppState addOption(AppOption option) {
            options.add(option);
            return this;
        }

        @Override
        public Iterator<AppOption> iterator() {
            return options.iterator();
        }

        @Override
        public AppNode current() {
            return stack.current();
        }

        @Override
        public AppState back() {
            pop();
            return this;
        }

        @Override
        public AppNode pop() {
            return stack.pop();
        }

        @Override
        public void push(AppNode item) {
            stack.push(item);
        }

        @Override
        public Object eval(String script) throws ScriptException {
            return scriptRunner.eval(script);
        }

        @Override
        public void put(String key, Object value) {
            scriptRunner.put(key, value);
        }

        @Override
        public Object get(String key) {
            return scriptRunner.get(key);
        }

        @Override
        public AppNode parse(TextInput input) {
            return parser.parse(input);
        }
    }

}
