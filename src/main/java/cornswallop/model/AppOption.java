package cornswallop.model;

import java.util.Map;
import java.util.OptionalInt;
import java.util.TreeMap;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

/**
 * Holds information about a user's selection.
 */
public interface AppOption {

    String text();
    String get(String attribute);
    void forEachAttribute(BiConsumer<String, String> consumer);
    Stream<Map.Entry<String, String>> attributes();

    static AppOptionBuilder create(String text) {
        return new AppOptionBuilder(text);
    }

    class AppOptionBuilder {
        final String text;
        final Map<String, String> attributes;
        OptionalInt key;

        public AppOptionBuilder(String text) {
            this.text = text;
            this.attributes = new TreeMap<>();
            this.key = OptionalInt.empty();
        }

        public AppOptionBuilder attr(String key, String value) {
            attributes.put(key, value);
            return this;
        }

        public AppOptionBuilder onKey(int ch) {
            key = OptionalInt.of(ch);
            return this;
        }

        public AppOption build() {
            final AppOption option = new AppOption() {
                @Override
                public void forEachAttribute(BiConsumer<String, String> consumer) {
                    attributes.forEach(consumer);
                }
                @Override
                public Stream<Map.Entry<String, String>> attributes() {
                    return attributes.entrySet().stream();
                }
                @Override
                public String text() {
                    return text;
                }
                @Override
                public String get(String attribute) {
                    return attributes.get(attribute);
                }
            };
            return key.isPresent() ? new KeyAwareOption(key.getAsInt(), option) : option;
        }
    }

    class KeyAwareOption implements AppOption, KeyAware {

        final int key;
        final AppOption base;

        public KeyAwareOption(int key, AppOption base) {
            this.key = key;
            this.base = base;
        }
        @Override
        public void forEachAttribute(BiConsumer<String, String> consumer) {
            base.forEachAttribute(consumer);
        }
        @Override
        public String text() {
            return base.text();
        }
        @Override
        public String get(String attribute) {
            return base.get(attribute);
        }
        @Override
        public Stream<Map.Entry<String, String>> attributes() {
            return base.attributes();
        }
        @Override
        public int getKey() {
            return key;
        }
    }

}
