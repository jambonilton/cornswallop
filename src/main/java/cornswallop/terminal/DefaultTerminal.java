package cornswallop.terminal;

import jline.console.ConsoleReader;

import java.io.IOException;
import java.io.PrintWriter;

import static cornswallop.text.StringUtil.wrap;

public class DefaultTerminal implements Terminal {

    private static final int DEFAULT_LINE_WIDTH = 80;
    private static final String PROMPT = AnsiUtil.coloured(">", TerminalColour.RED);

    private final PrintWriter out;
    private final jline.console.ConsoleReader reader;
    private final int lineWidth;

    public DefaultTerminal() throws IOException {
        reader = new ConsoleReader();
        reader.setPrompt(PROMPT);
        out = new PrintWriter(reader.getOutput());
        lineWidth = DEFAULT_LINE_WIDTH;

    }

    @Override
    public void println() {
        out.println();
        out.flush();
    }

    @Override
    public void println(String str) {
        out.println(wrap(str, lineWidth));
        out.flush();
    }

    @Override
    public void print(String str) {
        out.print(wrap(str, lineWidth));
        out.flush();
    }

    @Override
    public void clear() {
        try {
            reader.clearScreen();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int read() {
        try {
            return reader.readCharacter();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void prompt() {
        print(PROMPT);
    }

    @Override
    public String readLine() {
        try {
            return reader.readLine();
        } catch (IOException e) {
            throw new RuntimeException("Failed to read input from user!", e);
        }
    }
}
