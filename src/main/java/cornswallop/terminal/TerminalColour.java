package cornswallop.terminal;

public enum TerminalColour {

    BLACK,
    RED,
    GREEN,
    YELLOW,
    BLUE,
    MAGENTA,
    CYAN,
    WHITE,
    DEFAULT;

    public int value()
    {
        return ordinal();
    }

    public int fg()
    {
        return ordinal() + 30;
    }

    public int bg()
    {
        return ordinal() + 40;
    }

    public int fgBright()
    {
        return ordinal() + 90;
    }

    public int bgBright()
    {
        return ordinal() + 100;
    }

}
