package cornswallop.terminal;

public interface AnsiUtil {

    String ESCAPE = "\u001B[",
           M = "m",
           END = "0"+M;

    static String coloured(String text, TerminalColour colour) {
        return ESCAPE + colour.fgBright() + M + text + ESCAPE + END;
    }

}
