package cornswallop.terminal;

import cornswallop.model.AppOption;
import cornswallop.nlp.Thesaurus;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static cornswallop.util.CollectionUtil.toStream;

public class AdventureTermInput implements ViewInput {

    private static final Set<String> IGNORE_WORDS = Stream.of(
            "the", "a", "in", "with", "to", "out", "it", "he", "she"
    ).collect(Collectors.toSet());

    private final Terminal terminal;
    private final Thesaurus thesaurus;

    public AdventureTermInput(Terminal terminal, Thesaurus thesaurus) {
        this.terminal = terminal;
        this.thesaurus = thesaurus;
    }

    @Override
    public AppOption getInput(Iterable<AppOption> options) {
        Optional<AppOption> option;
        String line;
        while (!(option = getOptionFromInput(options, line = terminal.readLine())).isPresent())
            if (!line.isEmpty())
                terminal.println("I don't understand \"" + line + "\".");
        return option.get();
    }

    private Optional<AppOption> getOptionFromInput(Iterable<AppOption> options, String input) {
        return toStream(options)
                .filter(option -> optionMatchesInput(input, option))
                .findAny();
    }

    private boolean optionMatchesInput(String input, AppOption option) {
        final String[] requiredWords = option.text()
                .toLowerCase()
                .replaceAll("[^\\w\\s\\-']", "")
                .split("\\s+");
        final boolean containsAllRequiredWords = Stream.of(requiredWords)
                .filter(w -> !IGNORE_WORDS.contains(w))
                .allMatch(w -> inputContainsSameMeaningWord(input, w));
        return containsAllRequiredWords;
    }

    private boolean inputContainsSameMeaningWord(String input, String word) {
        return thesaurus.lookup(word)
                .filter(input::contains)
                .findAny().isPresent();
    }

}
