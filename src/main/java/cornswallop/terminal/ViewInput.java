package cornswallop.terminal;

import cornswallop.model.AppOption;

public interface ViewInput {

    AppOption getInput(Iterable<AppOption> options);

}
