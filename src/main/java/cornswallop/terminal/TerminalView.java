package cornswallop.terminal;

import cornswallop.media.AudioPlayer;
import cornswallop.media.BasicAudioPlayer;
import cornswallop.model.AppNode;
import cornswallop.model.AppOption;
import cornswallop.model.AppState;
import cornswallop.script.TemplateEvaluator;
import cornswallop.text.StringUtil;
import cornswallop.view.AppView;

import javax.script.ScriptException;
import java.util.Optional;

import static cornswallop.util.CollectionUtil.concat;

public class TerminalView implements AppView {

    private static final String DEFAULT_NODE_TYPE = "text";

    private final Terminal terminal;
    private final TemplateEvaluator templateEvaluator;
    private final AudioPlayer audio;
    private final ViewInput adventureInput, selectInput;

    public TerminalView(Terminal terminal,
                        TemplateEvaluator templateEvaluator,
                        ViewInput adventureInput,
                        ViewInput selectInput) {
        this.terminal = terminal;
        this.templateEvaluator = templateEvaluator;
        this.adventureInput = adventureInput;
        this.selectInput = selectInput;
        this.audio = new BasicAudioPlayer();
    }

    @Override
    public AppOption getInput(AppState state) {
        final AppNode current = state.current();
        if (current.hasOptions())
            terminal.clear();
        final String output = getOutput(state, current);
        terminal.println('\n' + output);
        return getNextOption(state);
    }


    private String getOutput(AppState state, AppNode node) {
        final String scriptAttribute = node.get("script");
        runScript(state, scriptAttribute);
        if (!StringUtil.isEmpty(node.get("sound")))
            audio.play(state.resolve(node.get("sound")).toFile());
        return templateEvaluator.run(state, node.text());
    }

    private void runScript(AppState state, String script) {
        if (!StringUtil.isEmpty(script)) {
            try {
                state.eval(script);
            } catch (ScriptException e) {
                e.printStackTrace();
            }
        }
    }

    private AppOption getNextOption(AppState state) {
        return getOptionFromNode(state, state.current())
                .orElseGet(()-> getNextOption(state.back()));
    }

    private Optional<AppOption> getOptionFromNode(AppState state, AppNode node) {
        final String nodeType = getType(node);
        switch (nodeType) {
            case "adventure":
                return Optional.of(adventureInput.getInput(concat(node, state)));
            case "select":
                return Optional.of(selectInput.getInput(concat(node, state)));
            case "exit":
                terminal.println();
                System.exit(0);
            default:
                return Optional.empty();
        }
    }

    private String getType(AppNode node) {
        return Optional.ofNullable(node.get("type")).map(String::toLowerCase).orElse(DEFAULT_NODE_TYPE);
    }

}
