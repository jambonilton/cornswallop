package cornswallop.terminal;

import cornswallop.model.AppOption;
import cornswallop.model.KeyAware;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static cornswallop.util.CollectionUtil.toStream;

public class SelectTermInput implements ViewInput {

    private static final char RIGHT = 'x', LEFT = 'z';
    private static final int PAGE_SIZE = 10;

    private final Terminal terminal;

    public SelectTermInput(Terminal terminal) {
        this.terminal = terminal;
    }

    @Override
    public AppOption getInput(Iterable<AppOption> options) {
        final int pageStart = 0;
        return pickFromList(unlistedOptions(options), listedOptions(options), pageStart);
    }

    private AppOption pickFromList(Map<Integer, AppOption> unlisted, List<AppOption> options, int start) {
        final int end = Math.min(start+PAGE_SIZE, options.size());
        printOptions(options, start, end);
        return getFromList(unlisted, options, start, end);
    }

    private Map<Integer, AppOption> unlistedOptions(Iterable<AppOption> options) {
        return toStream(options)
                .filter(option -> option instanceof KeyAware)
                .collect(Collectors.toMap(o -> ((KeyAware) o).getKey(), o -> o));
    }

    private List<AppOption> listedOptions(Iterable<AppOption> options) {
        return toStream(options)
                .filter(option -> !(option instanceof KeyAware))
                .collect(Collectors.toList());
    }

    private AppOption getFromList(Map<Integer, AppOption> unlisted, List<AppOption> options, int start, int end) {
        final Key key = new Key(terminal.read(), start);
        if (key.isNumber() && key.isInBounds(start, end))
            return options.get(key.asNumber());
        else if (key.get() == RIGHT && hasNextPage(options, start))
            return pickFromList(unlisted, options, start+PAGE_SIZE);
        else if (key.get() == LEFT && hasPreviousPage(start))
            return pickFromList(unlisted, options, start-PAGE_SIZE);
        else if (unlisted.containsKey(key.get()))
            return unlisted.get(key.get());
        return getFromList(unlisted, options, start, end);
    }

    private void printOptions(List<AppOption> options, int start, int end) {
        terminal.println();
        for (int i=start; i < end; i++)
            terminal.println(AnsiUtil.coloured(i-start+1+"", TerminalColour.RED) + ". " + options.get(i).text());
        if (hasPreviousPage(start) || hasNextPage(options, start)) {
            terminal.print(Character.toString(LEFT));
            for (int i=0; i < options.size(); i+=PAGE_SIZE)
                terminal.print(i == start ? "\u258a" : "\u2550");
            terminal.println(Character.toString(RIGHT));
        }
        terminal.prompt();
    }

    private boolean hasNextPage(List<AppOption> options, int start) {
        return start+PAGE_SIZE < options.size();
    }

    private boolean hasPreviousPage(int start) {
        return start-PAGE_SIZE >= 0;
    }

    static class Key {

        final int character, number;

        Key(int character, int start) {
            this.character = character;
            this.number = Character.getNumericValue(character) + start - 1;
        }

        boolean isNumber() {
            return number >= 0;
        }

        int get() {
            return character;
        }

        int asNumber() {
            return number;
        }

        boolean isInBounds(int start, int end) {
            return start <= number && number < end;
        }

    }

}
