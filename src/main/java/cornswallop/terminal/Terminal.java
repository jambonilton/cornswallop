package cornswallop.terminal;

/**
 * Simplified interface for sending text to and from a terminal.
 */
public interface Terminal {

    void println();

    void println(String str);

    void print(String str);

    String readLine();

    int read();

    void clear();

    void prompt();

}
