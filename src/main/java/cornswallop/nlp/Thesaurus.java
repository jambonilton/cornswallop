package cornswallop.nlp;

import java.util.stream.Stream;

/**
 * Lookup for words of same meaning.  Will always include input word.
 */
public interface Thesaurus {

    Stream<String> lookup(String word);

}
