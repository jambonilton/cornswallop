package cornswallop.nlp;

import cornswallop.text.CharIterator;
import cornswallop.text.TextInput;
import cornswallop.util.index.LongIndex;
import cornswallop.util.index.LongIndexMap;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.OptionalLong;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

import static cornswallop.util.CollectionUtil.toStream;
import static java.util.stream.Collectors.toSet;

/**
 * Accepts files of the format:
 *
 * abbreviate,abridge,abstract,attenuate,be telegraphic,blot out,blue-pencil,bob,boil down,bowdlerize,cancel,capsulize,censor,circumscribe,clip,coarct,compact,compress,concentrate,condense,consolidate,constrict,constringe,contract,cramp,crop,cross out,curtail,cut,cut back,cut down,cut off short,cut short,decrease,delete,dock,draw,draw in,draw together,edit,edit out,elide,epitomize,erase,expunge,expurgate,extenuate,foreshorten,kill,knit,mow,narrow,nip,omit,poll,pollard,prune,pucker,pucker up,purse,reap,recap,recapitulate,reduce,rescind,retrench,rub out,shave,shear,shorten,slash,snub,solidify,strangle,strangulate,strike,strike off,strike out,stunt,sum up,summarize,synopsize,take in,telescope,trim,truncate,void,waste no words,wrinkle
 * abbreviated,Spartan,abridged,abstracted,aposiopestic,bobbed,brief,brusque,capsule,capsulized,clipped,close,compact,compendious,compressed,concise,condensed,contracted,crisp,cropped,curt,curtailed,cut,cut short,digested,docked,elided,elliptic,epigrammatic,gnomic,laconic,mowed,mown,nipped,pithy,pointed,pollard,polled,pruned,reaped,reserved,sententious,shaved,sheared,short,short and sweet,short-cut,shortened,snub,snubbed,succinct,summary,synopsized,taciturn,terse,tight,to the point,trimmed,truncated
 * abbreviation,abbreviature,abrege,abridgment,abstract,apocope,aposiopesis,astriction,astringency,blue-penciling,bottleneck,bowdlerization,brief,cancellation,capsule,censoring,censorship,cervix,circumscription,clipping,coarctation,compactedness,compaction,compend,compression,compressure,concentration,condensation,condensed version,consolidation,conspectus,constriction,constringency,contraction,contracture,crasis,curtailment,cutting,decrease,deletion,digest,diminuendo,draft,editing,elision,ellipsis,epitome,erasure,expurgation,foreshortening,head,hourglass,hourglass figure,isthmus,knitting,narrow place,narrowing,neck,omission,outline,overview,pandect,precis,pruning,puckering,pursing,recap,recapitulation,reduction,retrenchment,review,rubric,shortened version,shortening,skeleton,sketch,solidification,stranglement,strangulation,striction,stricture,striking,summary,summation,survey,syllabus,syncope,syneresis,synopsis,systole,telescoping,thumbnail sketch,topical outline,truncation,wasp waist,wrinkling
 * abdicate,abandon,abjure,acknowledge defeat,be pensioned,be superannuated,cashier,cast,cease,cede,cry quits,demit,desist from,drop,forgo,forswear,give over,give up,hand over,have done with,jettison,lay down,leave,leave off,pension off,quit,reject,relinquish,renounce,renounce the throne,resign,retire,retire from office,scrap,shed,slough,stand aside,stand down,step aside,superannuate,surrender,throw away,throw out,throw up,vacate,waive,withdraw from,wrest,yield
 *
 * Where each line begins with the word to lookup, and is followed by words of similar meaning as a comma-delimited list.
 *
 * TODO logging, lenient exception handling
 */
public class FlatFileThesaurus implements Thesaurus {

    private static final String DESIRED_ACCESS = "r";
    private static final String CHARSET = "ascii";
    private static final char DELIM = ',';
    private static final Set<Character> NON_WORDS = Stream.of(DELIM, '\r', '\n').collect(toSet());

    private final RandomAccessFile file;
    private final LongIndex<String> index;

    public FlatFileThesaurus(File file) throws IOException {
        this(new RandomAccessFile(file, DESIRED_ACCESS), createIndex(file));
    }

    // Note, this will only work if the whole file is 1 byte characters
    private static LongIndex<String> createIndex(File file) {
        final LongIndex<String> index = new LongIndexMap<>();
        final AtomicLong count = new AtomicLong();
        try {
            Files.lines(file.toPath()).forEach(line -> {
                int endOfFirstWord = line.indexOf(',');
                if (endOfFirstWord < 0)
                    endOfFirstWord = line.length();
                index.setIndex(count.getAndAdd(line.length()+1), line.substring(0, endOfFirstWord));
            });
        } catch (IOException e) {
            throw new RuntimeException("Could not create index from file "+file, e);
        }
        return index;
    }

    public FlatFileThesaurus(RandomAccessFile file, LongIndex<String> index) {
        this.file = file;
        this.index = index;
    }

    @Override
    public Stream<String> lookup(String word) {
        final OptionalLong position = this.index.getIndex(word);
        return position.isPresent() ? getWordsFromIndex(position.getAsLong()) : Stream.of(word);
    }

    private Stream<String> getWordsFromIndex(long position) {
        try {
            final FileChannel channel = file.getChannel();
            channel.position(position);
            return toStream(wordIteratorFromChannel(channel))
                    .onClose(()->closeChannel(channel));
        } catch (IOException e) {
            throw new RuntimeException("Could not look up thesaurus word to to IO problem.", e);
        }
    }

    private static SplitLineWordIterator wordIteratorFromChannel(FileChannel channel) {
        final TextInput textInput = textInputFromChannel(channel);
        return new SplitLineWordIterator(textInput);
    }

    private static TextInput textInputFromChannel(FileChannel channel) {
        return new CharIterator(Channels.newReader(channel, CHARSET));
    }

    private static void closeChannel(FileChannel channel) {
        try {
            channel.close();
        } catch (IOException e) {
            throw new RuntimeException("Failed to close channel.", e);
        }
    }

    static class SplitLineWordIterator implements Iterator<String> {

        final TextInput in;

        public SplitLineWordIterator(TextInput in) {
            this.in = in;
        }

        @Override
        public boolean hasNext() {
            if (!in.hasNext())
                return false;
            final char peek = in.peek();
            if (peek == DELIM)
                in.next(); // burn comma
            return peek != '\r' && peek != '\n';
        }

        @Override
        public String next() {
            final String word = in.readUntil(NON_WORDS::contains);
            return word.trim();
        }

    }

}
