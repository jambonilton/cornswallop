package cornswallop.nlp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

public class OverrideThesaurus implements Thesaurus {

    private final Map<String, List<String>> override;
    private final Thesaurus base;

    public OverrideThesaurus(Thesaurus base) {
        this.override = new HashMap<>();
        this.base = base;
    }

    @Override
    public Stream<String> lookup(String word) {
        return override.containsKey(word) ? override.get(word).stream() : base.lookup(word);
    }

    public OverrideThesaurus override(String... synonyms) {
        if (synonyms.length > 0)
            override.put(synonyms[0], asList(synonyms));
        return this;
    }

}
