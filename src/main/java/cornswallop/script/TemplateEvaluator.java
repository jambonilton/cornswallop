package cornswallop.script;

import cornswallop.text.CharIterator;
import cornswallop.text.TextInput;

public interface TemplateEvaluator {
    default String run(ScriptRunner runner, String input) {
        return run(runner, new CharIterator(input));
    }
    String run(ScriptRunner runner, TextInput input);
}
