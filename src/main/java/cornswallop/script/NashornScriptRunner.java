package cornswallop.script;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class NashornScriptRunner implements ScriptRunner {

    public static final String ENGINE_NAME = "nashorn";

    private final ScriptEngine base;

    public NashornScriptRunner() {
        this(new ScriptEngineManager().getEngineByName(ENGINE_NAME));
    }

    public NashornScriptRunner(ScriptEngine base) {
        this.base = base;
    }

    @Override
    public Object eval(String script) throws ScriptException {
        return base.eval(script);
    }

    @Override
    public void put(String key, Object value) {
        base.put(key, value);
    }

    @Override
    public Object get(String key) {
        return base.get(key);
    }
}
