package cornswallop.script;

import cornswallop.text.TextInput;

import javax.script.ScriptException;
import java.util.Optional;

public class MoneySignBracesTemplateEvaluator implements TemplateEvaluator {

    @Override
    public String run(ScriptRunner runner, TextInput input) {
        final StringBuilder output = new StringBuilder();

        while (input.hasNext()) {
            output.append(input.readUntil('$'));
            if (!input.hasNext())
                break;
            if (input.skip().peek() == '{')
                runScript(runner, readScript(input.skip())).ifPresent(output::append);
            else
                output.append('$');
        }

        return output.toString();
    }

    private String readScript(TextInput input) {
        final ScriptInputBuilder script = new ScriptInputBuilder(input);
        while (script.read())
            ;
        return script.toString();
    }

    private Optional<Object> runScript(ScriptRunner runner, String script) {
        try {
            final Object result = runner.eval(script);
            return Optional.ofNullable(result);
        } catch (ScriptException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    private static class ScriptInputBuilder {

        final StringBuilder script = new StringBuilder();
        final TextInput input;

        int nesting = 0;

        public ScriptInputBuilder(TextInput input) {
            this.input = input;
        }

        boolean read() {
            if (!input.hasNext())
                throw new IllegalStateException("Unexpected end of input while reading script: "+script.toString());
            final char ch = input.next();
            if (isEnd(ch))
                return false;
            script.append(ch);
            return true;
        }

        boolean isEnd(char ch) {
            switch (ch) {
                case '}':
                    return nesting-- <= 0;
                case '{':
                    nesting++;
            }
            return false;
        }

        @Override
        public String toString() {
            return script.toString();
        }

    }

}
