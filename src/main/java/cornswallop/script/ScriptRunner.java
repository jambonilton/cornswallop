package cornswallop.script;

import javax.script.ScriptException;

public interface ScriptRunner {

    Object eval(String script) throws ScriptException;

    void put(String key, Object value);

    Object get(String key);

}
