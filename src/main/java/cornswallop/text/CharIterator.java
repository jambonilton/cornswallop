package cornswallop.text;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.CharBuffer;
import java.util.function.Predicate;

/**
 * Wrapper for reader implementation that allows for peeking ahead and managing other stately things.
 *
 * TODO too damn fat, deal with memory allocation
 */
public class CharIterator implements TextInput {

    public static final int DEFAULT_BUFFER_SIZE = 1024;

    final Reader reader;

    private CharBuffer buffer;

    public CharIterator(String str) {
        this(new StringReader(str));
    }

    public CharIterator(Reader reader) {
        this(reader, DEFAULT_BUFFER_SIZE);
    }

    public CharIterator(Reader reader, int bufferSize) {
        this.reader = reader;
        this.buffer = CharBuffer.allocate(bufferSize);
        buffer.limit(0);
    }

    @Override
    public Character peek() {
        return buffer.get(buffer.position());
    }

    @Override
    public String peekWhile(Predicate<Character> p) {
        buffer.mark();
        try {
            final StringBuilder sb = new StringBuilder();
            while (buffer.hasRemaining() && p.test(peek()))
                sb.append(buffer.get());
            return sb.toString();
        } finally {
            buffer.reset();
        }
    }

    @Override
    public Character next() {
        return buffer.get();
    }

    @Override
    public boolean hasNext() {
        return buffer.hasRemaining() || readBuffer();
    }

    @Override
    public String read(int nchars) {
        if (!hasRemaining(nchars))
            throw new IllegalArgumentException("Could not read "+nchars+", reached end of input.");
        try {
            return buffer.toString();
        } finally {
            advance(nchars);
        }
    }

    @Override
    public void advance(int newPosition) {
        buffer.position(buffer.position() + newPosition);
    }

    private boolean hasRemaining(int nchars) {
        if (nchars > buffer.capacity())
            throw new IllegalArgumentException("Expected "+nchars+" is too large for buffer!");
        else if (buffer.position() + nchars >= buffer.limit() && !readBuffer())
            return false;
        return true;
    }

    private boolean readBuffer() {
        try {
            buffer.compact();
            if (reader.read(buffer) == -1) {
                buffer.limit(buffer.position());
                buffer.position(0);
                return false;
            }
            buffer.flip();
            return true;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
