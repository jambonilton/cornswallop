package cornswallop.text;

import cornswallop.model.AppOption;

import static cornswallop.text.StringUtil.fromLastChar;
import static cornswallop.text.StringUtil.trimBefore;

public class CsfOptionParser implements Parser<AppOption> {

    @Override
    public AppOption parse(TextInput input) {

        if (!input.hasNext())
            return null;

        final AppOption.AppOptionBuilder option = AppOption.create(input.readLine());
        while (input.hasNext()) {
            final int indent = getIndent(input.readWhile(Character::isWhitespace));
            if (input.peek().equals('-'))
                break;
            final String key = input.readUntil(':');
            option.attr(key.trim(), getAttributeValue(indent, input.skip()));
        }
        return option.build();
    }

    private String getAttributeValue(int keyIndent, TextInput input) {
        final StringBuilder sb = new StringBuilder(trimBefore(input.readLine()));
        final int indent = getIndentFromLine(input);
        if (indent > keyIndent) {
            while (input.hasNext() && getIndentFromLine(input) >= indent)
                sb.append('\n').append(input.readLine().substring(indent));
        }
        return sb.toString();
    }

    private int getIndentFromLine(TextInput input) {
        return getIndent(fromLastChar(input.peekWhile(Character::isWhitespace), '\n'));
    }

    private int getIndent(String str) {
        return str.length();
    }

}
