package cornswallop.text;

import cornswallop.file.AppLinker;
import cornswallop.model.AppNode;
import cornswallop.util.InputUtil;

import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IncludingNodeParser implements LinkingNodeParser {

    private static final Pattern INCLUDE_PATTERN = Pattern.compile("\\$include\\(([^\\)]+)\\)");

    private final Parser<AppNode> base;
    private final AppLinker linker;

    public IncludingNodeParser(Parser<AppNode> base, AppLinker linker) {
        this.base = base;
        this.linker = linker;
    }

    @Override
    public Path resolve(String location) {
        return linker.resolve(location);
    }

    @Override
    public AppNode parse(TextInput input) {
        final AppNode node = base.parse(input);
        return nodeWithText(node, includeLinkedContent(node.text()));
    }

    private String includeLinkedContent(String input) {
        final StringBuffer text = new StringBuffer();
        final Matcher matcher = INCLUDE_PATTERN.matcher(input);
        while (matcher.find())
            matcher.appendReplacement(text, importTextFrom(matcher.group(1)));
        matcher.appendTail(text);
        return text.toString();
    }

    private String importTextFrom(String link) {
        final String fromFile = InputUtil.toString(linker.resolve(link).toFile());
        return includeLinkedContent(fromFile); // recursively follow includes.
    }

    private AppNode nodeWithText(AppNode node, String text) {
        final AppNode.AppNodeBuilder builder = AppNode.builder(text);
        node.forAttributes(builder::attr);
        node.forEach(builder::add);
        return builder.build();
    }

}