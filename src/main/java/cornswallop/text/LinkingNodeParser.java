package cornswallop.text;

import cornswallop.file.AppLinker;
import cornswallop.model.AppNode;

public interface LinkingNodeParser extends Parser<AppNode>, AppLinker {

    default AppNode parseFrom(String location) {
        return parse(resolve(location).toFile());
    }

}
