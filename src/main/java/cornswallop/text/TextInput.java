package cornswallop.text;

import cornswallop.util.PeekingIterator;

import java.util.function.Predicate;

/**
 * Interface for navigating text input easily.
 */
public interface TextInput extends PeekingIterator<Character> {

    String read(int nchars);
    void advance(int nchars);

    default String readLine() {
        final String line = readUntil('\n');
        skip();
        return line;
    }

    default String readUntil(char until) {
        return readWhile(c -> c.charValue() != until);
    }

    default String readUntil(Predicate<Character> p) {
        return readWhile(p.negate());
    }

    default String readWhile(Predicate<Character> p) {
        final StringBuilder sb = new StringBuilder();
        while (hasNext() && p.test(peek()))
            sb.append(next());
        return sb.toString();
    }

    String peekWhile(Predicate<Character> p);

    default String peekLine() {
        return peekUntil('\n');
    }

    default String peekUntil(char until) {
        return peekWhile(c -> c.charValue() != until);
    }

    default TextInput skip(CharSequence str) {
        final String read = read(str.length());
        if (!read.equals(str))
            throw new IllegalStateException("Expected \""+str+"\" but was \""+read+"\".");
        return this;
    }

    default TextInput skip() {
        if (hasNext())
            next();
        return this;
    }

    default TextInput skipLine() {
        return skipWhile(c -> c.charValue() != '\n' && c.charValue() != '\r');
    }

    default TextInput skipWhitespace() {
        return skipWhile(Character::isWhitespace);
    }

    default TextInput skipUntil(char until) {
        return skipWhile(c -> c.charValue() != until);
    }

    default TextInput skipUntil(Predicate<Character> p) {
        return skipWhile(p.negate());
    }

    default TextInput skipWhile(Predicate<Character> p) {
        while (hasNext() && p.test(peek()))
            next();
        return this;
    }

}
