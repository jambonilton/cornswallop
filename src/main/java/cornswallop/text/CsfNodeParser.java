package cornswallop.text;

import cornswallop.model.AppNode;
import cornswallop.model.AppOption;

import java.util.Map;
import java.util.TreeMap;

public class CsfNodeParser implements Parser<AppNode> {

    private final Parser<AppOption> optionParser = new CsfOptionParser();

    @Override
    public AppNode parse(TextInput input) {

        final Map<String, String> attributes = new TreeMap<>();
        while (input.hasNext() && input.peekLine().trim().matches("[\\w\\s\\-]+:.*"))
            attributes.put(input.skipWhitespace().readUntil(':').trim(), input.skip().readLine().trim());

        final StringBuilder text = new StringBuilder();
        while (input.hasNext()) {
            text.append(input.readWhile(Character::isWhitespace));
            if (input.peek() == '-')
                break;
            text.append(input.readLine()).append('\n');
        }

        final AppNode.AppNodeBuilder nodeBuilder = AppNode.builder(text.toString()).attrs(attributes);

        AppOption option;
        while ((option = getOption(input)) != null)
            nodeBuilder.add(option);

        return nodeBuilder.build();
    }

    private AppOption getOption(TextInput input) {

        final StringBuilder sb = new StringBuilder();
        sb.append(removeLeadingBullet(input.readLine()));
        while (input.hasNext() && !input.peekLine().trim().startsWith("-"))
            sb.append('\n').append(input.readLine());
        return optionParser.parse(sb.toString());
    }

    private String removeLeadingBullet(String line) {
        return line.replace("-", "").trim();
    }

}
