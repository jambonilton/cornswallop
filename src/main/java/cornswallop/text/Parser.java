package cornswallop.text;

import java.io.*;

/**
 * Turns text into stuff.
 */
public interface Parser<T> {

    default T parse(File file) {
        try (Reader reader = new FileReader(file)) {
            return parse(reader);
        } catch (IOException e) {
            throw new RuntimeException("Failed to parse file!", e);
        }
    }

    default T parse(String str) {
        return parse(new StringReader(str));
    }

    default T parse(Reader reader) {
        return parse(new CharIterator(reader));
    }

    T parse(TextInput input);

}
