package cornswallop.text;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import static java.lang.Character.isWhitespace;

/**
 * String-related utility functions.
 */
public interface StringUtil {

    static boolean isEmpty(String str) {
        return str == null || str.isEmpty();
    }

    static String wrap(String str, int lineSize) {
        if (isEmpty(str) || lineSize >= str.length() || lineSize <= 0)
            return str;
        final int breakPoint = IntStream.generate(new AtomicInteger(lineSize)::getAndDecrement)
                .limit(lineSize)
                .filter(i -> isWhitespace(str.charAt(i)))
                .findFirst().orElse(lineSize);
        return str.subSequence(0, breakPoint).toString()
                + '\n' + wrap(str.substring(breakPoint).trim(), lineSize);
    }

    static String trimBefore(String str) {
        int i;
        for (i=0; i < str.length() && Character.isWhitespace(str.charAt(i)); i++)
            ;
        return str.substring(i);
    }

    static String toChar(String str, int ch) {
        final int index = str.indexOf(ch);
        return index >= 0 ? str.substring(0, index) : str;
    }

    static String fromChar(String str, int ch) {
        final int index = str.indexOf(ch);
        return index >= 0 ? str.substring(index) : str;
    }

    static String fromLastChar(String str, int ch) {
        final int index = str.lastIndexOf(ch);
        return index >= 0 ? str.substring(index) : str;
    }

    static String unwrap(String str) {
        return str.replaceAll("(?<!\n)\n(?!\n) *", "").trim();
    }

}
