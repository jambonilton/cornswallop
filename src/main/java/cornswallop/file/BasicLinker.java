package cornswallop.file;

import java.nio.file.Path;

public class BasicLinker implements AppLinker {

    private Path folder;

    public BasicLinker(Path folder) {
        this.folder = folder;
    }

    @Override
    public Path resolve(String path) {
        final Path resolved = folder.resolve(path);
        folder = resolved.toFile().isDirectory() ? resolved : resolved.getParent();
        return resolved;
    }

    public AppLinker immutable() {
        return path -> folder.resolve(path);
    }

}
