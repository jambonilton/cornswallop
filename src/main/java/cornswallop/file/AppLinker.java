package cornswallop.file;

import java.nio.file.Path;

public interface AppLinker {

    Path resolve(String location);

}
