package cornswallop.connector;

import cornswallop.file.AppLinker;
import cornswallop.model.AppNode;
import cornswallop.model.AppOption;
import cornswallop.model.AppState;
import cornswallop.text.Parser;

import java.io.File;

public class LinkConnector implements AppConnector, AttributeAware {

    public static final String LINK_ATTRIBUTE = "link";

    final AppLinker linker;
    final Parser<AppNode> parser;

    public LinkConnector(AppLinker linker, Parser<AppNode> parser) {
        this.linker = linker;
        this.parser = parser;
    }

    @Override
    public String getAttribute() {
        return LINK_ATTRIBUTE;
    }

    @Override
    public AppNode getNext(AppState state, AppOption option) {
        final String location = option.get(LINK_ATTRIBUTE);
        final File file = linker.resolve(location).toFile();
        return parser.parse(file);
    }

}
