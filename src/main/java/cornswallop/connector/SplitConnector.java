package cornswallop.connector;

import cornswallop.attributes.AttributeInterpreter;
import cornswallop.model.AppNode;
import cornswallop.model.AppOption;
import cornswallop.model.AppState;

import java.util.Map;

public class SplitConnector implements AppConnector {

    private final Map<String, AttributeInterpreter> interpreters;
    private final Map<String, AppConnector> connectors;

    public SplitConnector(Map<String, AttributeInterpreter> interpreters, Map<String, AppConnector> connectors) {
        this.interpreters = interpreters;
        this.connectors = connectors;
    }

    @Override
    public AppNode getNext(AppState state, AppOption option) {
        option.forEachAttribute((k, v) -> {
            if (interpreters.containsKey(k))
                interpreters.get(k).execute(state, v);
        });
        return option.attributes().map(Map.Entry::getKey)
                .filter(connectors::containsKey)
                .map(connectors::get)
                .findFirst()
                .map(c -> c.getNext(state, option))
                .orElse(state.current());
    }

}
