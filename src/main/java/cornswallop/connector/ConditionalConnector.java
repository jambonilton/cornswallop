package cornswallop.connector;

import cornswallop.model.AppNode;
import cornswallop.model.AppOption;
import cornswallop.model.AppState;
import cornswallop.text.Parser;

import javax.script.ScriptException;

import static cornswallop.text.StringUtil.fromChar;
import static cornswallop.text.StringUtil.toChar;

public class ConditionalConnector implements AppConnector, AttributeAware {

    private final AppConnector base;
    private final Parser<AppOption> optionParser;

    public ConditionalConnector(AppConnector base, Parser<AppOption> optionParser) {
        this.base = base;
        this.optionParser = optionParser;
    }

    @Override
    public String getAttribute() {
        return "if";
    }

    @Override
    public AppNode getNext(AppState state, AppOption option) {
        if (eval(state, toChar(option.get("if"), '\n')))
            return base.getNext(state, optionParser.parse(fromChar(option.get("if"), '\n')));
        // TODO handle multiple else if
        if (option.get("else if") != null && eval(state, toChar(option.get("else if"), '\n')))
            return base.getNext(state, optionParser.parse(fromChar(option.get("else if"), '\n')));
        if (option.get("else") != null)
            return base.getNext(state, optionParser.parse(option.get("else")));
        else
            return state.current();
    }

    private boolean eval(AppState state, String script) {
        try {
            final Object eval = state.eval(script);
            if (eval instanceof Boolean)
                return (Boolean) eval;
            else if (eval instanceof Number)
                return ((Number) eval).intValue() > 0;
            else if (eval instanceof String)
                return !((String) eval).trim().isEmpty();
            return eval != null;
        } catch (ScriptException e) {
            // TODO log me better
            return false;
        }
    }

}
