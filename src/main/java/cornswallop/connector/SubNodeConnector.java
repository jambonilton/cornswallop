package cornswallop.connector;

import cornswallop.model.AppNode;
import cornswallop.model.AppOption;
import cornswallop.model.AppState;

import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static cornswallop.text.StringUtil.unwrap;

public class SubNodeConnector implements AppConnector, AttributeAware {

    static final Pattern ATTR_PATTERN = Pattern.compile("([\\w\\s\\-]+):(.*)\n");

    @Override
    public String getAttribute() {
        return "node";
    }

    @Override
    public AppNode getNext(AppState state, AppOption option) {
        final String nodeString = option.get(getAttribute());
        final AppNode node = parseNode(nodeString);
        return node;
    }

    // TODO use actual parser
    static AppNode parseNode(String nodeString) {
        final Map<String, String> attributes = new TreeMap<>();
        final Matcher matcher = ATTR_PATTERN.matcher(nodeString);
        int lastIndex = 0;
        while (matcher.find()) {
            attributes.put(matcher.group(1).trim(), matcher.group(2).trim());
            lastIndex = matcher.end();
        }
        final String nodeText = unwrap(nodeString.substring(lastIndex));
        return AppNode.builder(nodeText).attrs(attributes).build();
    }

}
