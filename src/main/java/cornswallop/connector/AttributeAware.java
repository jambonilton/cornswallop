package cornswallop.connector;

public interface AttributeAware {

    String getAttribute();

}
