package cornswallop.connector;

import cornswallop.model.AppNode;
import cornswallop.model.AppOption;
import cornswallop.model.AppState;

public class ExitConnector implements AppConnector, AttributeAware {

    @Override
    public String getAttribute() {
        return "exit";
    }

    @Override
    public AppNode getNext(AppState state, AppOption option) {
        return AppNode.builder(option.get(getAttribute())).attr("type", "exit").build();
    }

}
