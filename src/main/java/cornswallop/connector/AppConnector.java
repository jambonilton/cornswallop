package cornswallop.connector;

import cornswallop.model.AppNode;
import cornswallop.model.AppOption;
import cornswallop.model.AppState;

public interface AppConnector {

    AppNode getNext(AppState state, AppOption option);

}
