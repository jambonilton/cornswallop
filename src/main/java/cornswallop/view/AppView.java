package cornswallop.view;

import cornswallop.model.AppOption;
import cornswallop.model.AppState;

public interface AppView {

    AppOption getInput(AppState state);

}
