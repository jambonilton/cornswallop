package cornswallop.attributes;

import cornswallop.model.AppState;

public interface AttributeInterpreter {

    void execute(AppState state, String value);

}
