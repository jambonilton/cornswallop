package cornswallop.attributes;

import cornswallop.media.AudioPlayer;
import cornswallop.model.AppState;

public class MusicInterpreter implements AttributeInterpreter {

    private final AudioPlayer audioPlayer;

    public MusicInterpreter(AudioPlayer audioPlayer) {
        this.audioPlayer = audioPlayer;
    }

    @Override
    public void execute(AppState state, String filename) {
        audioPlayer.loop(state.resolve(filename).toFile());
    }

}
