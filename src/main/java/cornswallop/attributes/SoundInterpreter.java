package cornswallop.attributes;

import cornswallop.media.AudioPlayer;
import cornswallop.model.AppState;

public class SoundInterpreter implements AttributeInterpreter {

    private final AudioPlayer audioPlayer;

    public SoundInterpreter(AudioPlayer audioPlayer) {
        this.audioPlayer = audioPlayer;
    }

    @Override
    public void execute(AppState state, String filename) {
        audioPlayer.play(state.resolve(filename).toFile());
    }

}
