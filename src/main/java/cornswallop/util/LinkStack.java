package cornswallop.util;

import java.util.LinkedList;

public class LinkStack<E> implements Stack<E> {

    final LinkedList<E> list;

    public LinkStack(E root) {
        this.list = new LinkedList<>();
        this.list.push(root);
    }

    @Override
    public E current() {
        return list.element();
    }

    @Override
    public E pop() {
        return list.pop();
    }

    @Override
    public void push(E item) {
        list.push(item);
    }



}
