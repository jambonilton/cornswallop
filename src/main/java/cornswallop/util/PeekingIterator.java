package cornswallop.util;

import java.util.Iterator;

/**
 * Iterator that allows 1 position lookahead.
 */
public interface PeekingIterator<E> extends Iterator<E> {

    E peek();

}
