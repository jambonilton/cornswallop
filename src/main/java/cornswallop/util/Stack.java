package cornswallop.util;

/**
 * Simplified stack interface.
 */
public interface Stack<E> {

    E current();

    E pop();

    void push(E item);

}
