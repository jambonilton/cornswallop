package cornswallop.util;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

public class ArrayIterator<E> implements Iterator<E> {

    final E[] arr;
    AtomicInteger index = new AtomicInteger();

    public ArrayIterator(E[] arr) {
        this.arr = arr;
    }

    @Override
    public boolean hasNext() {
        return index.get() < arr.length;
    }

    @Override
    public E next() {
        return arr[index.getAndIncrement()];
    }
}
