package cornswallop.util;

import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Collection-related utility functions.
 */
public interface CollectionUtil {

    static <T> Stream<T> toStream(Iterable<T> iterable) {
        return toStream(iterable.iterator());
    }

    static <T> Stream<T> toStream(Iterator<T> iterator) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator,
                Spliterator.ORDERED | Spliterator.NONNULL), false);
    }

    static <T> Iterable<T> concat(Iterable<T>... iterables) {
        return () -> {
            final Iterator<T>[] iterators = Stream.of(iterables)
                    .map(Iterable::iterator)
                    .toArray(Iterator[]::new);
            return concat(iterators);
        };
    }

    static <T> Iterator<T> concat(Iterator<T>... iterators) {
        return new IteratorChain<>(new ArrayIterator<>(iterators));
    }

    static <T> List<T> toList(Iterable<T> iterable) {
        return toStream(iterable).collect(Collectors.toList());
    }

}
