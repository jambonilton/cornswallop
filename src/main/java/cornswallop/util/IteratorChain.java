package cornswallop.util;

import java.util.Collections;
import java.util.Iterator;

public class IteratorChain<E> implements Iterator<E> {

    final Iterator<Iterator<E>> iterators;
    Iterator<E> current;

    public IteratorChain(Iterator<Iterator<E>> iterators) {
        this.iterators = iterators;
        this.current = Collections.emptyIterator();
    }

    @Override
    public boolean hasNext() {
        final boolean b = iterators.hasNext();
        return current.hasNext() || b && hasNextIterator();
    }

    private boolean hasNextIterator() {
        while (!current.hasNext() && iterators.hasNext())
            current = iterators.next();
        return current.hasNext();
    }

    @Override
    public E next() {
        return current.next();
    }
}
