package cornswallop.util;

import java.io.*;

public class InputUtil {

    public static String toString(File file) {
        try {
            return toString(new FileReader(file));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static String toString(Reader in) {
        final char[] buffer = new char[512];
        final StringBuilder sb = new StringBuilder();
        try {
            int read;
            while ((read = in.read(buffer)) > 0)
                sb.append(buffer, 0, read);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return sb.toString();
    }

}
