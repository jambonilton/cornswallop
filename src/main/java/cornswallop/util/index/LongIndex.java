package cornswallop.util.index;

import java.util.OptionalLong;

/**
 * Basic lookup for long indices.
 */
public interface LongIndex<T> {

    OptionalLong getIndex(T item);

    void setIndex(long value, T item);

}
