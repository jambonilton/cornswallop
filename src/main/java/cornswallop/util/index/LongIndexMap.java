package cornswallop.util.index;

import java.util.HashMap;
import java.util.Map;
import java.util.OptionalLong;

public class LongIndexMap<T> implements LongIndex<T> {

    private final Map<T, Long> map;

    public LongIndexMap() {
        this(new HashMap<>());
    }

    public LongIndexMap(Map<T, Long> map) {
        this.map = map;
    }

    @Override
    public OptionalLong getIndex(T item) {
        final Long index = map.get(item);
        return index == null ? OptionalLong.empty() : OptionalLong.of(index);
    }

    @Override
    public void setIndex(long value, T item) {
        map.put(item, value);
    }

}
