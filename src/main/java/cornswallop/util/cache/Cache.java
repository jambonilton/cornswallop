package cornswallop.util.cache;

import java.util.HashMap;
import java.util.function.Function;

public interface Cache<K,V> {

    V get(K key);

    void remove(K key);

    /**
     * Create a simple in-memory cache that uses the given function to load new items.
     */
    static <K,V> Cache<K,V> create(Function<K,V> loader) {
        return new MapCache<>(new HashMap<>(), loader);
    }

}
