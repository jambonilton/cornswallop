package cornswallop.util.cache;

import java.util.Map;
import java.util.function.Function;

/**
 * Basic in-memory cache.
 */
class MapCache<K,V> implements Cache<K,V> {

    private final Map<K,V> base;
    private final Function<K,V> loader;

    public MapCache(Map<K, V> base, Function<K, V> loader) {
        this.base = base;
        this.loader = loader;
    }

    @Override
    public V get(K key) {
        final V value = base.get(key);
        return value == null ? loader.apply(key) : value;
    }

    @Override
    public void remove(K key) {
        base.remove(key);
    }

}
