package cornswallop.util;

import java.util.HashMap;
import java.util.Map;

public abstract class Maps {

    public static <K,V> Map<K,V> hashMap(K key, V value, Object... others) {
        final Map<K, V> map = new HashMap<>();
        map.put(key, value);
        for (int i=0; i+1 < others.length; i+=2)
            map.put((K) others[i], (V) others[i+1]);
        return map;
    }

}
